FROM python:3.7-slim
ARG INLINE_SCAN_HASH=faa7e742ebe21270bb758eeb2491fcd1941c41b65f78363295b0f64bdbadc5f7
ARG INLINE_SCAN_VERSION=v0.6.1

COPY requirements.txt /

RUN apt-get -y update && \
    apt-get -y install curl && \
    pip install -r /requirements.txt && \
    apt-get -y clean

RUN curl -fSL -o /inline_scan "https://ci-tools.anchore.io/inline_scan-${INLINE_SCAN_VERSION}" && \
    echo "$INLINE_SCAN_HASH *inline_scan" | sha256sum -c - && \
    chmod +x /inline_scan
    
#RUN curl -s https://ci-tools.anchore.io/inline_scan-v0.6.1 -o /inline_scan && \
#    chmod +x /inline_scan

COPY data /
COPY pipe /
COPY pipe.yml /

ENTRYPOINT ["python3", "/pipe.py"]

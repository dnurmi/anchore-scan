# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.2

- patch: test for new version

## 0.0.1

- patch: initial release test


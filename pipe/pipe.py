import os
import re
import json
import uuid
import docker
import requests
import subprocess
from enum import Enum, IntEnum
from bitbucket_pipes_toolkit import Pipe, get_logger

logger = get_logger()

schema = {
    'IMAGE_NAME': {'type': 'string', 'required': True},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
    'VULN_REPORT_SEVERITY_CUTOFF': {'type': 'string', 'required': False, 'default': 'HIGH'},
    'CUSTOM_POLICY_PATH': {'type': 'string', 'required': False, 'default': '/critical_security_policy.json'},
    'ENABLE_VULN_REPORT': {'type': 'boolean', 'required': False, 'default': True},
    'ENABLE_POLICY_REPORT': {'type': 'boolean', 'required': False, 'default': True},
}

class Severity(IntEnum):
    UNKNOWN = 0
    NEGLIGIBLE = 1
    LOW = 2
    MEDIUM = 3
    HIGH = 4
    CRITICAL = 5

class AnchorePipe(Pipe):
    annotation_data_el = {
        "annotation_type": "VULNERABILITY",
        "external_id": None,  # required
        "summary": None,  # required
        "details": None,
        "severity": None,
        "result": None,
        "link": None,
    }

    vulnerability_report_template = {
        "report_type": "SECURITY",
        "external_id": None,  # required
        "title": "Anchore: Container Vulnerabilities",  # required
        "details": "Results of Anchore Container Image Scanner vulnerability analysis.",  # required
        "result": None,
        "reporter": "Created by Anchore Container Image Scanner.",
    }

    policy_report_template = {
        "report_type": "SECURITY",
        "external_id": None,  # required
        "title": "Anchore: Container Policy Findings",  # required
        "details": "Results of Anchore Container Image Scanner policy evaluation.",  # required
        "result": None,
        "reporter": "Created by Anchore Container Image Scanner.",
    }

    def _get_annotations_from_policyeval(self, eval_report_data):
        annotation_data_reports = []
        final_action = 'GO'

        findings = list(list(eval_report_data[0][list(eval_report_data[0].keys())[0]].values())[0][0]['detail']['result']['result'].values())[0]['result']['rows']
        count = 0
        for finding in findings:
            finding_el = {}
            finding_el.update(self.annotation_data_el)

            finding_el['external_id'] = str(count)
            finding_el['summary'] = "{}/{} - {}".format(finding[3], finding[4], finding[2])

            finding_el['details'] = finding[5]
            if finding[6].lower() == 'stop':
                sev = 'HIGH'
                result = 'FAILED'
            elif finding[6].lower() == 'warn':
                sev = 'MEDIUM'
                result = 'PASSED'
            elif finding[6].lower() == 'go':
                sev = 'LOW'
                result = 'PASSED'

            finding_el['severity'] = sev
            finding_el['result'] = result
            annotation_data_reports.append(finding_el)

            count = count + 1            

        try:
            final_action = list(list(eval_report_data[0].values())[0].values())[0][0]['detail']['result']['final_action'].lower()
        except Exception as err:
            logger.error("Failed to extract final action from policy evaluation report - exception: {}".format(err))
            final_action = 'stop'

        return(annotation_data_reports, final_action)

    def _get_annotations_from_vulns(self, vuln_report_data):
        annotation_data_reports = []

        count = 0
        highest_sev = Severity.UNKNOWN
        for vuln in vuln_report_data.get('vulnerabilities', []):
            try:
                sev = Severity[vuln.get('severity', 'UNKNOWN').upper()]
            except AttributeError as err:
                sev = Severity['UNKNOWN']
            if sev > highest_sev:
                highest_sev = sev

            finding_el = {}
            finding_el.update(self.annotation_data_el)

            # required
            finding_el['external_id'] = str(count)
            finding_el['summary'] = "{} ({})".format(vuln.get('vuln', "NA"), vuln.get('package', "NA"))

            # optional
            package_meta = {}
            for key in ['package_name', 'package_version', 'package_path']:
                vval = vuln.get(key, None)
                val = "NA"
                if vval and vval not in ['None']:
                    val = vval
                package_meta[key] = val

            finding_el['details'] = "Package={} PackageVersion={} PackagePath={}".format(package_meta.get('package_name', "NA"), package_meta.get('package_version', "NA"), package_meta.get('package_path', "NA"))
            #finding_el['path'] = ""
            if sev <= Severity['LOW']:
                el_sev = Severity['LOW']
            elif sev >= Severity['HIGH']:
                el_sev = Severity['HIGH']
            else:
                el_sev = sev
            finding_el['severity'] = el_sev.name
            finding_el['link'] = vuln.get('url', "NA")
            finding_el['result'] = "PASSED"
            if sev > Severity[self.severity_cutoff]:
                finding_el['result'] = "FAILED"

            annotation_data_reports.append(finding_el)

            count = count + 1
        return(annotation_data_reports, highest_sev)

    def _create_report(self, report_data):
        new_report = {}

        proxies = {
            "http": "http://host.docker.internal:29418",
        }
        url = "http://api.bitbucket.org/2.0/repositories/{}/{}/commit/{}/reports/{}".format(self.bb_account, self.bb_repo, self.bb_commit, report_data.get('uuid', report_data.get('external_id')))        
        try:
            r = requests.put(url, proxies=proxies, json=report_data)
            new_report = json.loads(r.content)
        except Exception as err:
            logger.error("Failed to create report - exception: {}".format(err))
            #raise err
            
        return new_report

    def _create_annotation(self, report_id, annotation_data):
        new_annotation = {}

        proxies = {
            "http": "http://host.docker.internal:29418",
        }
        url = "http://api.bitbucket.org/2.0/repositories/{}/{}/commit/{}/reports/{}/annotations/{}".format(self.bb_account, self.bb_repo, self.bb_commit, report_id, annotation_data.get('uuid', annotation_data.get('external_id')))
        try:
            r = requests.put(url, proxies=proxies, json=annotation_data)
            new_annotation = json.loads(r.content)
        except Exception as err:
            logger.error("Failed to create annotation - exception: {}".format(err))
            #raise err
            
        return new_annotation

    def _create_bulk_annotations(self, report_id, annotations):
        new_annotations = []

        chunk_size = 100
        chunks = [annotations[x:x+chunk_size] for x in range(0, len(annotations), chunk_size)]
        proxies = {
            "http": "http://host.docker.internal:29418",
        }
        url = "http://api.bitbucket.org/2.0/repositories/{}/{}/commit/{}/reports/{}/annotations".format(self.bb_account, self.bb_repo, self.bb_commit, report_id)
        try:
            for chunk in chunks:
                r = requests.post(url, proxies=proxies, json=chunk)
                new_annotations.extend(json.loads(r.content))
        except Exception as err:
            logger.error("Failed to create annotations - exception: {}".format(err))
            #raise err
            
        return new_annotations
    
    def run(self):
        super().run()

        # defaults
        self.anchore_report_path = "./anchore-reports"
        self.anchore_scan_cmd_path = "/inline_scan"

        # input settings/overrides
        self.image_name = self.get_variable('IMAGE_NAME')
        self.enable_policy_report = self.get_variable('ENABLE_POLICY_REPORT')
        self.enable_vuln_report = self.get_variable('ENABLE_VULN_REPORT')
        self.severity_cutoff = self.get_variable('VULN_REPORT_SEVERITY_CUTOFF').upper()
        self.policy_bundle_path = self.get_variable('CUSTOM_POLICY_PATH')
        if not os.path.exists(self.policy_bundle_path):
            self.policy_bundle_path = os.path.join(os.environ.get("BITBUCKET_CLONE_DIR"), self.get_variable('CUSTOM_POLICY_PATH'))

        logger.debug("from input CUSTOM_POLICY_PATH ({}) using actual policy bundle path ({})".format(self.get_variable('CUSTOM_POLICY_PATH'), self.policy_bundle_path))
            

        # environment settings
        self.bb_account = self.bb_username = os.environ.get("BITBUCKET_REPO_OWNER")
        self.bb_repo = os.environ.get("BITBUCKET_REPO_SLUG")
        self.bb_commit = os.environ.get("BITBUCKET_COMMIT")
        
        logger.info("Executing anchore image scan pipe")

        logger.info("Verifying access to image {}".format(self.image_name))
        try:
            client = docker.DockerClient(base_url=os.environ.get("DOCKER_HOST"))
            logger.debug("connecting to docker using DOCKER_HOST={} to find input image {}".format(os.environ.get("DOCKER_HOST"), self.image_name))
            image = client.images.get(self.image_name)
            if not image:
                raise Exception("could not get local image {}".format(self.image_name))
            
        except Exception as err:
            logger.error("Failed to stage docker image for analysis - exception: {}".format(err))
            raise err

        logger.info("Performing image analysis (this may take a few minutes)...")
        try:
            if os.path.exists("{}".format(self.anchore_scan_cmd_path)):
                cmdstr = "/inline_scan scan -b {} -r {}".format(self.policy_bundle_path, self.image_name)
                cmd = cmdstr.split()


                logger.debug("running anchore inline scan command: {}".format(' '.join(cmd)))
                os.environ["ANCHORE_CI_IMAGE"] = "docker.io/anchore/inline-scan-slim:latest"
                pipes = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                o, e = pipes.communicate()
                exitcode = pipes.returncode
                soutput = o
                serror = e
                logger.debug("command stdout: {}".format(soutput))
                logger.debug("command stderr: {}".format(serror))

                if exitcode == 0:
                    logger.info("Analysis complete.")
                else:
                    raise Exception("analysis failed due to non-zero exit code from anchore scan command")

            else:
                raise Exception("anchore scan command not found: {}".format(self.anchore_scan_cmd_path))
        except Exception as err:
            logger.error("Failed to execute local scan - exception: {}".format(err))
            raise err

        logger.info("Verifying and loading scan results.")
        vuln_report_data = eval_report_data = None
        try:
            if os.path.exists(self.anchore_report_path):
                vuln_report_file = policy_report_file = None
                for f in os.listdir(self.anchore_report_path):
                    if "-vuln.json" in f:
                        vuln_report_file = os.path.join(self.anchore_report_path, f)
                    elif "-policy.json" in f:
                        policy_report_file = os.path.join(self.anchore_report_path, f)

                if vuln_report_file and os.path.exists(vuln_report_file):
                    with open(vuln_report_file, 'r') as FH:
                        vuln_report_data = json.loads(FH.read())

                if policy_report_file and os.path.exists(policy_report_file):
                    with open(policy_report_file, 'r') as FH:
                        eval_report_data = json.loads(FH.read())
            else:
                msg = "no results found in directory {} after anchore inline scan execution".format(self.anchore_report_path)
                raise Exception(msg)
        except Exception as err:
            logger.error("Failed to find or parse anchore scan results - exception: {}".format(err))
            raise err

        logger.info("Creating Code Insights reports.")
        try:
            if eval_report_data and self.enable_policy_report:
                annotation_data_reports, final_action = self._get_annotations_from_policyeval(eval_report_data)
                report_result = "PASSED"
                if final_action in ['stop']:
                    report_result = "FAILED"

                # choose an external_id (required) and other report metadata based on env
                report_data = {}
                report_data.update(self.policy_report_template)
                report_data['result'] = report_result

                if self.bb_account and self.bb_repo and self.bb_commit:
                    external_id = "anchore_policy_report_{}".format(self.bb_commit)
                    report_data['external_id'] = external_id
                    
                    # create new report
                    new_report = self._create_report(report_data)
                    logger.debug("created new report - content: {}".format(new_report))

                    # populate report with findings annotations
                    report_id = new_report.get('external_id', None)
                    if report_id:
                        new_annotations = self._create_bulk_annotations(report_id, annotation_data_reports)
                        logger.debug("created new annotations - content: {}".format(new_annotations))
                else:
                    logger.warning("Not running in bitbucket environment - displaying report to stdout")
                    print("Evaluation Report: {}".format(json.dumps(annotation_data_reports, indent=4)))
                    
            else:
                logger.info("Skipping policy eval report generation")

            if vuln_report_data and self.enable_vuln_report:
                annotation_data_reports, highest_sev = self._get_annotations_from_vulns(vuln_report_data)
                # code here to set the 'passed' or 'failed' decision for top level report
                report_result = "PASSED"
                if highest_sev > Severity[self.severity_cutoff]:
                    report_result = "FAILED"

                report_data = {}
                report_data.update(self.vulnerability_report_template)
                report_data['result'] = report_result

                if self.bb_account and self.bb_repo and self.bb_commit:
                    external_id = "anchore_vulnerability_report_{}".format(self.bb_commit)
                    report_data['external_id'] = external_id
                    
                    # create new report
                    new_report = self._create_report(report_data)
                    logger.debug("created new report - content: {}".format(new_report))                

                    # populate report with findings annotations
                    report_id = new_report.get('external_id', None)

                    if report_id:
                        new_annotations = self._create_bulk_annotations(report_id, annotation_data_reports)
                        logger.debug("created new annotations - content: {}".format(new_annotations))                    
                else:
                    logger.warning("Not running in bitbucket environment - displaying report to stdout")
                    print("Vulnerability Report: {}".format(json.dumps(annotation_data_reports, indent=4)))
                    
            else:
                logger.info("No vulnerability results present - skipping vulnerability report generation")

        except Exception as err:
            logger.error("Failed to create reports/annotations - exception: {}".format(err))
            raise err
            
        self.success(message="Success!")

if __name__ == '__main__':
    pipe = AnchorePipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
